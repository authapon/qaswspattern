package qaswspattern

import (
	"errors"
	moosql "gitlab.com/authapon/moosqlite"
	"strings"
)

func GetWSPatternDB() ([]WSPatternDB, error) {
	WSPattern := make([]WSPatternDB, 0)
	db, err := moosql.GetSQL()
	if err != nil {
		return WSPattern, errors.New("DB connection fail!")
	}
	defer db.Close()
	con, _ := db.Begin()
	defer con.Commit()

	st, _ := con.Prepare("select `pattern` from `wscorrection`;")
	rows, err := st.Query()
	if err != nil {
		return WSPattern, errors.New("DB query fail!")
	}
	defer rows.Close()

	for rows.Next() {
		pattern := ""
		rows.Scan(&pattern)
		patternSplit := strings.Split(pattern, " ")
		tag := patternSplit[len(patternSplit)-1]
		patternX := strings.Join(patternSplit[:len(patternSplit)-1], " ")
		datax := WSPatternDB{Pattern: patternX, Tag: tag}
		WSPattern = append(WSPattern, datax)
	}
	return WSPattern, nil
}
