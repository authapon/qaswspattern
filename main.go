package qaswspattern

import (
	"fmt"
	crflib "gitlab.com/authapon/qascrflib"
	"strings"
)

type (
	WSPatternDB struct {
		Pattern string
		Tag     string
	}
)

func WSPatternIdent(data string, dict crflib.Words) string {
	if data == "" {
		return ""
	}

	WSPattern, err := GetWSPatternDB()
	if err != nil {
		return ""
	}

	longestPattern := GetLongestWSPattern(WSPattern)
	dataTag := crflib.GetWordTag(data)
	dataOut := crflib.CrfTrainSentence{}
	dataOut.Sentence = []string{}
	dataOut.Label = []string{}
	index := 0
	apply := false
	fmt.Printf("Start -> %s\n", data)
	for {
		if index >= len(dataTag.Sentence) {
			fmt.Printf("Finnished\n")
			break
		}
	ChkPoint:
		for i := longestPattern; i > 0; i-- {
			for i2 := range WSPattern {
				patternTag := strings.Split(WSPattern[i2].Pattern, " ")
				if len(patternTag) == i {
					fmt.Printf("Evaluated -> %s -> %s\n", WSPattern[i2].Pattern, WSPattern[i2].Tag)
					if WSPatternApply(dataTag, patternTag, WSPattern[i2].Tag, index, dict) {
						fmt.Printf("Match -> %s -> %s\n", WSPattern[i2].Pattern, WSPattern[i2].Tag)
						dataX := ""
						for ix := range patternTag {
							dataX += dataTag.Sentence[index+ix]
						}
						dataOut.Sentence = append(dataOut.Sentence, dataX)
						dataOut.Label = append(dataOut.Label, WSPattern[i2].Tag)
						index += i
						apply = true
						break ChkPoint
					}
					fmt.Printf("Not match !!!\n")
				}
			}
		}
		if !apply {
			dataOut.Sentence = append(dataOut.Sentence, dataTag.Sentence[index])
			dataOut.Label = append(dataOut.Label, dataTag.Label[index])
			index += 1
			fmt.Printf("**** No Evaluated ****\n")
		} else {
			apply = false
		}
	}

	return crflib.WordTagString(dataOut)
}

func WSPatternApply(data crflib.CrfTrainSentence, pattern []string, tag string, index int, dict crflib.Words) bool {
	if len(data.Sentence[index:]) < len(pattern) {
		return false
	}

	word := ""

	for i := 0; i < len(pattern); i++ {
		if pattern[i] == data.Label[index+i] {
			word += data.Sentence[index+i]
			continue
		} else {
			return false
		}
	}

	pos, ok := dict[word]
	if !ok {
		return false
	}
	for i := range pos {
		if pos[i] == tag {
			return true
		}
	}

	return false
}

func GetLongestWSPattern(nDB []WSPatternDB) int {
	longest := 0
	for k := range nDB {
		words := strings.Split(nDB[k].Pattern, " ")
		long := len(words)
		if long > longest {
			longest = long
		}
	}
	return longest
}
